const UserHistory = require("../models").user_game_histories;
const UserBiodata = require("../models").user_game_biodatas;
const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models").user_game;

module.exports = {
  getUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;
    const user = await User.findByPk(userId, {
      include: [{ model: UserBiodata }, { model: UserHistory }],
    });
    res.status(200).json({
      user: user,
    });
  },

  getUsers: async (req, res) => {
    const users = await User.findAll({
      include: [{ model: UserBiodata }],
    });

    res.status(200).json({
      users: users,
    });
  },

  createUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const { username, password } = req.body;
    let userRoleId = 1;
    if (req.body.isAdmin) {
      userRoleId = 2;
    }

    const encryptedPassword = await bcrypt.hash(password, 12);

    const newUser = await User.create({
      username,
      password: encryptedPassword,
      role_id: userRoleId,
    });

    const token = jwt.sign({ username: req.body.username }, "secret", {
      expiresIn: 3600,
    });

    res.status(200).json({
      user: newUser,
      token,
    });
  },

  updateUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;

    if (req.user.id !== userId) {
      return res.status(401).json({
        message: "Unauthorized",
      });
    }

    const { username, password } = req.body;

    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    const updatedUser = await User.update(
      {
        username: username,
        password: password,
      },
      {
        where: {
          id: userId,
        },
        returning: true,
      }
    );

    res.status(200).json({
      message: "User successfully updated",
      beforeUpdate: user,
      afterUpdate: updatedUser[1][0],
    });
  },

  deleteUser: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const userId = req.params.id;

    if (req.user.id !== userId) {
      return res.status(401).json({
        message: "Unauthorized",
      });
    }

    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    await User.destroy({
      where: {
        id: userId,
      },
    });

    res.status(200).json({
      message: "User successfully deleted",
      deletedUser: user,
    });
  },

  login: async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array(),
      });
    }

    const user = await User.findOne({ where: { username: req.body.username } });

    const isEqual = await bcrypt.compare(req.body.password, user.password);
    if (!user || !isEqual) {
      return res.status(403).json({
        message: "Wrong Username / Password",
      });
    }

    const token = jwt.sign({ username: req.body.username }, "secret", {
      expiresIn: 3600,
    });

    return res.status(200).json({
      message: "Login Success",
      token,
    });
  },
};
