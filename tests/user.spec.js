const request = require("supertest");
const app = require("../app");
const { faker } = require("@faker-js/faker");

const User = require("../models").user_game;
const UserBiodata = require("../models").user_game_biodatas;
const UserHistory = require("../models").user_game_histories;

const username = faker.internet.userName();
const password = faker.internet.password();

describe("Get Users Function", () => {
  test("200 Success", async () => {
    const response = await request(app).get("/api/user");
    const users = await User.findAll({
      include: [{ model: UserBiodata }],
      raw: true,
    });

    expect(response.statusCode).toBe(200);
    response.body.users.forEach((user, idx) => {
      expect(user.username).toBe(users[idx].username);
      expect(user.password).toBe(users[idx].password);
    });
  });
});

describe("Create User Function", () => {
  test("200 Success", async () => {
    const response = await request(app).post(`/api/user/`).send({
      username,
      password,
    });
    const newUser = await User.findOne({ where: { username: username } });

    expect(response.statusCode).toBe(200);
    expect(response.body.user.username).toEqual(newUser.username);
    expect(response.body.user.password).toEqual(newUser.password);
  });

  test("422 Validation Error", async () => {
    const response = await request(app)
      .post(`/api/user/`)
      .send({
        username: +username,
        password: +password,
      });

    expect(response.statusCode).toBe(422);
  });
});

describe("Get User Function", () => {
  test("200 Success", async () => {
    const userId = 1;
    const response = await request(app).get(`/api/user/${userId}`);
    const user = await User.findByPk(userId, {
      include: [{ model: UserBiodata }, { model: UserHistory }],
    });

    expect(response.statusCode).toBe(200);
    expect(JSON.stringify(response.body.user)).toEqual(JSON.stringify(user));
  });

  test("422 Validation Error", async () => {
    const response = await request(app).get("/api/user/test");

    expect(response.statusCode).toBe(422);
  });
});

describe("User Login Function", () => {
  test("200 Success", async () => {
    const response = await request(app).post("/api/user/login").send({
      username: username,
      password: password,
    });

    expect(response.statusCode).toBe(200);
    expect(response.body.message).toEqual("Login Success");
  });

  test("403 Auth Failed", async () => {
    const response = await request(app).post("/api/user/login").send({
      username: faker.internet.userName(),
      password: faker.internet.password(),
    });

    expect(response.statusCode).toBe(403);
    expect(response.body.message).toEqual("Wrong Username / Password");
  });

  test("422 Validation Error", async () => {
    const response = await request(app).post("/api/user/login").send({
      username: username,
    });

    expect(response.statusCode).toBe(422);
  });
});

describe("Update User Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const newPassword = faker.internet.password();

    const response = await request(app).patch(`/api/user/${user.id}`).send({
      password: newPassword,
    });

    const userAfterUpdate = await User.findOne({
      where: { username: username },
    });

    expect(response.statusCode).toBe(200);
    expect(response.body.beforeUpdate.username).toEqual(user.username);
    expect(response.body.beforeUpdate.password).toEqual(user.password);

    expect(response.body.afterUpdate.username).toEqual(
      userAfterUpdate.username
    );
    expect(response.body.afterUpdate.password).toEqual(
      userAfterUpdate.password
    );
  });

  test("404 Not Found", async () => {
    const newPassword = faker.internet.password();

    const response = await request(app).patch(`/api/user/0`).send({
      password: newPassword,
    });

    expect(response.statusCode).toBe(404);
    expect(response.body.message).toBe("User not found");
  });

  test("422 Validation Error", async () => {
    const newPassword = faker.internet.password();

    const response = await request(app).patch(`/api/user/test`).send({
      password: newPassword,
    });

    expect(response.statusCode).toBe(422);
  });
});

describe("Delete User Function", () => {
  test("200 Success", async () => {
    const user = await User.findOne({ where: { username: username } });

    const response = await request(app).delete(`/api/user/${user.id}`);

    expect(response.statusCode).toBe(200);
    expect(response.body.deletedUser.username).toEqual(user.username);
    expect(response.body.deletedUser.password).toEqual(user.password);
  });

  test("404 Not Found", async () => {
    const response = await request(app).delete(`/api/user/0`);

    expect(response.statusCode).toBe(404);
    expect(response.body.message).toBe("User not found");
  });

  test("422 Validation Error", async () => {
    const response = await request(app).delete(`/api/user/test`);

    expect(response.statusCode).toBe(422);
  });
});
