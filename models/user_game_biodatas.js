"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_biodatas.belongsTo(models.user_game, {
        foreignKey: "user_id",
        onDelete: "CASCADE",
      });
    }
  }
  user_game_biodatas.init(
    {
      user_id: { type: DataTypes.INTEGER, allowNull: false },
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      country: DataTypes.STRING,
      email: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user_game_biodatas",
    }
  );
  return user_game_biodatas;
};
