const JwtStrategy = require("passport-jwt").Strategy;
const User = require("../models").user_game;
const passport = require("passport");
const { ExtractJwt } = require("passport-jwt");

var cookieExtractor = (req) => {
  var token = null;
  if (req.cookies && req.cookies["token"]) {
    token = req.cookies["token"];
  }
  return token;
};

const jwtStrategy = new JwtStrategy(
  {
    jwtFromRequest: cookieExtractor,
    secretOrKey: "secret",
  },
  async (payload, done) => {
    let user;
    try {
      user = await User.findOne({ where: { username: payload.username } });
    } catch (error) {
      return done(err, false);
    }
    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  }
);

const jwtStrategyAPI = new JwtStrategy(
  {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: "secret",
  },
  async (payload, done) => {
    let user;
    try {
      user = await User.findOne({ where: { username: payload.username } });
    } catch (error) {
      return done(err, false);
    }
    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  }
);

passport.serializeUser(function (user, done) {
  done(null, user);
});

module.exports = {
  jwtStrategy,
  jwtStrategyAPI,
};
