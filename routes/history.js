const router = require("express").Router();
const { body, query, param } = require("express-validator");
const historyController = require("../controllers/history");
const passport = require("passport");
const jwtStrategyAPI = require("../utils/passport").jwtStrategyAPI;


router.get("/", historyController.getHistories);
router.get(
  "/:userId",
  [
    param("userId")
      .notEmpty()
      .isNumeric()
      .withMessage("Valid userId param is required"),
  ],
  historyController.getHistory
);
router.post(
  "/:userId",
  [
    param("userId")
      .notEmpty()
      .isNumeric()
      .withMessage("Valid userId param is required"),
    body(["title", "publisher"])
      .notEmpty()
      .withMessage("Title and Publisher body field is required"),
  ],
  historyController.createHistory
);
router.patch(
  "/:gameId",
  passport.authenticate(jwtStrategyAPI, { session: false }),
  [
    param("gameId")
      .notEmpty()
      .isNumeric()
      .withMessage("Valid gameId query is required"),
  ],
  historyController.updateHistory
);
router.delete(
  "/:gameId",
  passport.authenticate(jwtStrategyAPI, { session: false }),
  [
    param("gameId")
      .notEmpty()
      .isNumeric()
      .withMessage("Valid gameId params is required"),
  ],
  historyController.deleteHistory
);

module.exports = router;
